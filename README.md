Basic lesson D
--------------

- ExampleIf.java (example if, check for details)
- ExampleIfElse.java (example else, check for details)
- ExampleSwitch.java (example switch, check for details)

Topic covered (if branch)
-------------------------

- if(condition){...dostuff}
- if condition is true do stuff, if not skip branch and go to next line

Topic covered (if branch example)
---------------------------------

    if(condition){
     // do this if condition true
    }
    // go here if condition false

Topic covered (if else branch)
------------------------------

- if(condition){...dostuff}else{...dootherstuff}
- if condition is true do stuff, if condition is false do other stuff

Topic covered (if else branch example)
--------------------------------------

    if(condition){
        // condition true do this
    }else{
        // condition false do this
    }

Topic covered (switch branch)
-----------------------------

- multiple cases
- test one condition
- can have multiple values

Topic covered (switch branch example)
-------------------------------------

    switch(n){
        case 1:
            // n is 1
            // do stuff
            break;
        case 2:
            // n is 2
            // do stuff
            break;
        default:
            // no other case matched
            // do stuff
            break;
    }

