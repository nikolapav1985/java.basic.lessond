/**
*
* public class ExampleIf
*
*/
public class ExampleIf{
    public static void main(String args[]){
        int a,b;

        if(args.length != 2){
            System.exit(1);
        }

        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        
        if(a>b){
            System.out.println(String.format("Greater number if %d and lesser number is %d.",a,b));
            System.exit(0);
        }

        if(a==b){
            System.out.println(String.format("Numbers %d and %d are equal.",a,b));
            System.exit(0);
        }

        System.out.println(String.format("Greater number is %d and lesser number is %d.",b,a));
    }
}
