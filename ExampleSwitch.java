/**
*
* public class ExampleSwitch
*
*/
public class ExampleSwitch{
    public static void main(String args[]){
        int a,b,option;

        if(args.length != 3){
            System.exit(1);
        }

        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        option=Integer.parseInt(args[2]);

        switch(option){
            case 1:
                System.out.println(String.format("Numbers %d and %d are equal? %s.",a,b,a==b));
                break;
            case 2:
                System.out.println(String.format("Number %d is less than %d? %s.",a,b,a<b));
                break;
            case 3:
                System.out.println(String.format("Number %d is more than %d? %s.",a,b,a>b));
                break;
            default:
                break;
        }
    }
}
